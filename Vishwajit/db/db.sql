create table Emp(
    empid integer primary key auto_increment,
    name varchar(50),
    salary double,
    age integer
);

insert into Emp values(default,"omkar",15500,23);
insert into Emp values(default,"Suyog",17000,21);
insert into Emp values(default,"abhi",16000,22);